import Vue from 'vue'

Vue.component('b-btn', {
  render: function (h) {
    var renderTag
    if (this.href) {
      renderTag = 'a'
    } else if (this.to) {
      renderTag = 'router-link'
    } else {
      renderTag = 'button'
    }
    return h(
      renderTag, {
        attrs: {
          href: this.href,
          to: this.to,
          type: this.buttonType,
        },
        staticClass: 'bttn',
      },
      this.$slots.default, // массив потомков
    )
  },
  props: {
    href: String,
    to: String,
    buttonType: {
      type: String,
      validator: function (value) {
        return ['submit', 'reset', 'button'].indexOf(value) !== -1
      }
    }  
  }
})