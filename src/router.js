import Vue from 'vue';
import Router from 'vue-router';
import BoardPage from './views/BoardPage.vue';
import EcocardPage from './views/EcocardPage.vue';
import EcocardCatalog from './app/EcocardCatalog.vue';
import EcocardSearch from './app/EcocardSearch.vue';
import EcocardCardsList from './app/EcocardCardsList.vue';
import EcocardCardDetail from './app/EcocardCardDetail.vue';
import EcocardBranchDetail from './app/EcocardBranchDetail.vue';
import InfoAbout from './app/InfoAbout.vue';
import InfoBrand from './app/InfoBrand.vue';
import InfoConfidentiality from './app/InfoConfidentiality.vue';
import InfoFaq from './app/InfoFaq.vue';
import InfoHelp from './app/InfoHelp.vue';
import InfoManifesto from './app/InfoManifesto.vue';
import InfoOffer from './app/InfoOffer.vue';
import InfoPage from './views/InfoPage.vue';
import InfoTerms from './app/InfoTerms.vue';
import LandingPage from './views/LandingPage.vue';
import LoginPage from './views/LoginPage.vue';
import NotFound from './views/NotFound.vue';
import ProfilePage from './views/ProfilePage.vue';
import RegisterPage from './views/RegisterPage.vue';
import RegistrationChoose from './views/RegistrationChoose.vue';
import PersonalPage from './views/PersonalPage.vue';
import PersonalProperties from './app/PersonalProperties.vue';
import PersonalReferral from './app/PersonalReferral.vue';
import PersonalBill from './app/PersonalBill.vue';
import PersonalInterests from './app/PersonalInterests.vue';
import PersonalSettings from './app/PersonalSettings.vue';
import PersonalEcocardList from './app/PersonalEcocardList.vue';
import PersonalEcocardDetail from './app/PersonalEcocardDetail.vue';
import PersonalEcocardCreate from './app/PersonalEcocardCreate.vue';
import PersonalEcocardSubscribe from './app/PersonalEcocardSubscribe.vue';
import PersonalEcocardEmployees from './app/PersonalEcocardEmployees.vue';
import PersonalBannerList from './app/PersonalBannerList.vue';
import PersonalBannerAdd from './app/PersonalBannerAdd.vue';

import PersonalCompanyRegister from './app/PersonalCompanyRegister.vue';

import PersonalBannerAddStep1 from './app/PersonalBannerAddStep1.vue';
import PersonalBannerAddStep2 from './app/PersonalBannerAddStep2.vue';
import PersonalBannerAddStep3 from './app/PersonalBannerAddStep3.vue';
import PersonalBannerAddStep4 from './app/PersonalBannerAddStep4.vue';
import PersonalBannerAddStep5 from './app/PersonalBannerAddStep5.vue';
import PersonalBannerAddStepFinal from './app/PersonalBannerAddStepFinal.vue';
import PersonalBannerArchive from './app/Archive.vue';


import PromoApmboard from './views/PromoApmboard.vue';
import PromoApmecocard from './views/PromoApmecocard.vue';


import AdvertizerPage from './views/AdvertizerPage.vue';



import TechIcons from './app/tech/TechIcons.vue';
import TechNotification from './app/tech/TechNotification.vue';
import Tech from './app/tech/Tech.vue';


Vue.use(Router);


const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    return savedPosition;
  } else {
    return {
      x: 0,
      y: 0
    }
  }
};

const router = new Router({
  scrollBehavior,
  mode: 'history',
  routes: [{
      path: '/tech',
      name: 'tech',
      component: Tech,
      children: [{
          path: 'icons',
          name: 'icons',
          component: TechIcons
        },
        {
          path: 'notification',
          name: 'notifications',
          component: TechNotification
        },
      ]
    },
    {
      path: '/info',
      redirect: '/info/about'
    },
    {
      path: '/personal',
      redirect: '/personal/properties'
    },
    {
      path: '/',
      name: 'landing',
      component: LandingPage
    },
    {
      path: '/apmboard',
      name: 'promo apmboard',
      component: PromoApmboard
    },
    {
      path: '/apmecocard',
      name: 'promo apmecocard',
      component: PromoApmecocard
    },
    {
      path: '/profile',
      name: 'profile',
      component: ProfilePage,
      meta: {
        title: 'Профиль'
      }
    },
    {
      path: '/advertizer',
      name: 'advertizer',
      component: AdvertizerPage,
    },
    {
      path: '/ecocard',
      name: 'ecocard',
      component: EcocardPage,
      meta: {
        title: 'Ecocard'
      },
      children: [{
          path: 'catalog',
          name: 'catalog',
          component: EcocardCatalog,
          meta: {
            breadcrumb: 'Каталог'
          }
        },
        {
          path: 'catalog/:section/list',
          name: 'catalog section list',
          component: EcocardCardsList,
          props: true,
          meta: {}
        },
        {
          path: 'catalog/:section',
          name: 'catalog sections',
          component: EcocardCatalog,
          meta: {}
        },
        {
          path: 'search',
          name: 'ecocard search',
          component: EcocardSearch,
          meta: {}
        },
        {
          path: ':id',
          name: 'ecocard detail page',
          component: EcocardCardDetail,
          props: true,
          meta: {}
        },
        {
          path: ':ecocardId/:branchId',
          name: 'ecocard branch page',
          component: EcocardBranchDetail,
          props: true,
          meta: {}
        }
      ]
    },
    {
      path: '/board',
      name: 'board',
      component: BoardPage
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage
    },
    {
      path: '/registration-choose',
      name: 'registration-choose',
      component: RegistrationChoose
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterPage
    },
    {
      path: '/info',
      name: 'info',
      component: InfoPage,
      meta: {
        breadcrumb: 'Информация'
      },
      children: [{
          path: 'about',
          name: 'about',
          component: InfoAbout,
          meta: {
            breadcrumb: 'О нас'
          }
        },
        {
          path: 'manifesto',
          name: 'manifesto',
          component: InfoManifesto,
          meta: {
            breadcrumb: 'Манифест'
          }
        },
        {
          path: 'brand',
          name: 'brand',
          component: InfoBrand,
          meta: {
            breadcrumb: 'Бренд'
          }
        },
        {
          path: 'faq',
          name: 'faq',
          component: InfoFaq,
          meta: {
            breadcrumb: 'FAQ'
          }
        },
        {
          path: 'offer',
          name: 'offer',
          component: InfoOffer,
          meta: {
            breadcrumb: 'Оферта'
          }
        },
        {
          path: 'terms',
          name: 'terms',
          component: InfoTerms,
          meta: {
            breadcrumb: 'Пользовательское соглашение'
          }
        },
        {
          path: 'confidentiality',
          name: 'confidentiality',
          component: InfoConfidentiality,
          meta: {
            breadcrumb: 'Конфиденциальность'
          }
        },
        {
          path: 'help',
          name: 'help',
          component: InfoHelp,
          meta: {
            breadcrumb: 'Помощь'
          }
        }
      ]
    },
    {
      path: '/personal',
      name: 'personal',
      component: PersonalPage,
      meta: {
        breadcrumb: 'Личный кабинет'
      },
      children: [{
          path: 'properties',
          name: 'properties',
          component: PersonalProperties,
          meta: {
            breadcrumb: 'Личный данные',
            topMenu: true
          }
        },
        {
          path: 'referral',
          name: 'referral',
          component: PersonalReferral,
          meta: {
            breadcrumb: 'Партнерская программа',
            topMenu: true
          }
        },
        {
          path: 'bill',
          name: 'bill',
          component: PersonalBill,
          meta: {
            breadcrumb: 'Обзор счета',
            topMenu: true
          }
        },
        {
          path: 'interests',
          name: 'interests',
          component: PersonalInterests,
          meta: {
            breadcrumb: 'Интересы',
            topMenu: true
          }
        },
        {
          path: 'settings',
          name: 'settings',
          component: PersonalSettings,
          meta: {
            breadcrumb: 'Настройки',
            topMenu: true
          }
        },
        {
          path: 'company/register',
          name: 'company register',
          component: PersonalCompanyRegister,
          meta: {
            breadcrumb: 'Регистрация компании'
          }
        },
        {
          path: 'ecocard',
          name: 'admin ecocards list',
          component: PersonalEcocardList,
          meta: {
            breadcrumb: 'Дисконтные программы'
          },
        },
        {
          path: 'ecocard/create',
          name: 'admin create ecocard',
          component: PersonalEcocardCreate,
          meta: {
            breadcrumb: 'Создать программу'
          }
        },
        {
          path: 'ecocard/subscribe',
          name: 'admin ecocard subscribe',
          component: PersonalEcocardSubscribe,
          meta: {
            breadcrumb: 'Подписка'
          }
        },
        {
          path: 'ecocard/employees',
          name: 'admin ecocard employees',
          component: PersonalEcocardEmployees,
          meta: {
            breadcrumb: 'Сотрудники'
          }
        },
        {
          path: 'ecocard/:ecocardId',
          name: 'admin ecocard detail',
          component: PersonalEcocardDetail,
          meta: {
            breadcrumb: 'Настройки'
          }
        },
        {
          path: 'banner',
          name: 'personal banners list',
          component: PersonalBannerList,
          meta: {
            breadcrumb: 'Рекламные объявления'
          }
        },
        {
          path: 'banner/archive',
          name: 'personal banners archive',
          component: PersonalBannerArchive,
          meta: {
            breadcrumb: 'Рекламные объявления'
          }
        },
        {
          path: 'banner/add',
          redirect: 'banner/add/step1',
          name: 'add banner',
          component: PersonalBannerAdd,
          meta: {
            breadcrumb: 'Дать рекламу'
          },
          children: [{
              path: 'step1',
              name: 'banner add step 1',
              component: PersonalBannerAddStep1,
              meta: {
                breadcrumb: 'Подача рекламы первый шаг'
              }
            },
            {
              path: 'step2',
              name: 'banner add step 2',
              component: PersonalBannerAddStep2,
              meta: {
                breadcrumb: 'Подача рекламы второй шаг'
              }
            },
            {
              path: 'step3',
              name: 'banner add step 3',
              component: PersonalBannerAddStep3,
              meta: {
                breadcrumb: 'Подача рекламы третий шаг'
              }
            },
            {
              path: 'step4',
              name: 'banner add step 4',
              component: PersonalBannerAddStep4,
              meta: {
                breadcrumb: 'Подача рекламы четвертый шаг'
              }
            },
            {
              path: 'step5',
              name: 'banner add step 5',
              component: PersonalBannerAddStep5,
              meta: {
                breadcrumb: 'Подача рекламы пятый шаг'
              }
            },
            {
              path: 'step6',
              name: 'banner add step final',
              component: PersonalBannerAddStepFinal,
              meta: {
                breadcrumb: 'Подача рекламы финальный шаг'
              }
            },
          ]
        },
      ]
    },
    {
      path: '*',
      component: NotFound
    }
  ],

});


// router.beforeResolve((to, from, next) => {
//   // If this isn't an initial page load.
//   if (to.name) {
//     // Start the route progress bar.
//     NProgress.start()
//   }
//   next()
// })

// router.afterEach((to, from) => {
//   // Complete the animation of the route progress bar.
//   NProgress.done()
// })

export default router