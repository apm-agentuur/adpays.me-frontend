import '@babel/polyfill'
import 'es6-promise/auto'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VTooltip from 'v-tooltip'
import Vue2Filters from 'vue2-filters'
import VueLazyload from 'vue-lazyload'
import VueScrollTo from 'vue-scrollto'
import VueWaypoint from 'vue-waypoint'
import VueMasonry from 'vue-masonry-css'
import VModal from 'vue-js-modal'
import * as VueGoogleMaps from 'vue2-google-maps'
import StarRating from 'vue-star-rating'
import SocialSharing from 'vue-social-sharing'
import Viewer from 'v-viewer'
import VueProgressiveImage from 'vue-progressive-image'
import Notifications from 'vue-notification'

import rus from 'vee-validate/dist/locale/ru'
import VeeValidate, {Validator} from 'vee-validate'
import inViewportDirective from 'vue-in-viewport-directive'
Vue.directive('in-viewport', inViewportDirective)

//tech components
// import 'prismjs'
// import 'prismjs/themes/prism.css'

// import all icons
import './app/icons/_globals'

//import base components
import './app/_base'
import 'viewerjs/dist/viewer.css'

import './sass/main.sass'

const VueUploadComponent = require('vue-upload-component')
Vue.component('file-upload', VueUploadComponent)
Vue.use(VTooltip)
Vue.use(Vue2Filters)
Vue.use(VueLazyload)
Vue.use(VueScrollTo)
Vue.use(VueWaypoint)
Vue.use(VueMasonry)
Vue.use(VModal, {
  dynamic: true
})
Vue.component('star-rating', StarRating)
Vue.use(SocialSharing);
Vue.use(VueProgressiveImage)
Vue.use(Notifications)
Vue.use(Viewer, {
  defaultOptions: {
    toolbar: false,
    navbar: false,
    title: false
  }
})
Vue.use(VueGoogleMaps, {
  installComponents: true,
  load: {
    key: 'AIzaSyCG-1ZNg2FOF5rRbTBcjoemG718_JJXxUU',
    libraries: 'places',
  },
})

Validator.localize('ru', rus)
const config = {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  errorBagName: 'validateErrors', // change if property conflicts
  events: 'input|blur',
  fieldsBagName: 'fields',
  i18n: null, // the vue-i18n plugin instance
  i18nRootKey: 'validations', // the nested key under which the validation messsages will be located
  inject: true,
  locale: 'rus',
  strict: true,
  validity: false,
};

Vue.use(VeeValidate, config)

NProgress.configure({
  showSpinner: false
});

Vue.directive('click-outside', {
  bind(el, binding, vnode) {
    el.event = function (event) {
      // here I check that click was outside the el and his childrens
      if (!(el === event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.event);
  },
  unbind(el) {
    document.body.removeEventListener('click', el.event);
  }
})

Vue.filter('prettyBytes', function (num) {
  // jacked from: https://github.com/sindresorhus/pretty-bytes
  if (typeof num !== 'number' || isNaN(num)) {
    throw new TypeError('Expected a number');
  }

  var exponent;
  var unit;
  var neg = num < 0;
  var units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  if (neg) {
    num = -num;
  }

  if (num < 1) {
    return (neg ? '-' : '') + num + ' B';
  }

  exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
  num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
  unit = units[exponent];

  return (neg ? '-' : '') + num + ' ' + unit;
});



export default new Vue({
  router,
  store,
  el: "#app",
  data() {
    return {
      errors: null,
      messages: null,
      mainMenuSeen: false,
      globalAuth: false,
      loading: false,
      boardFirstVisit: false,
      mobile: window.innerWidth <= 1023,
      isApp: false
    };
  },
  created() {
    addEventListener("resize", () => {
      this.mobile = innerWidth <= 1023;
    });
  },
  render: h => h(App)
})