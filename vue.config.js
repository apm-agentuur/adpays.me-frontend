module.exports = {
  lintOnSave: false,
  configureWebpack: {
   resolve: {
     alias: {
       'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
     }
   }
  },
  productionSourceMap: true,
    css: {
      // extract CSS in components into a single CSS file (only in production)
      extract: true,

      // enable CSS source maps?
      sourceMap: true
    }
}
