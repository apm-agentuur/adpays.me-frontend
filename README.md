# Adpays.me frontend git

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

Install all npm packages.

```
npm i
```

### Running

Running dev server

```
npm run serve
```